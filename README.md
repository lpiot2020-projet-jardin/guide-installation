# GardenIOT : Installation

# Installation

Il faudra installer :

- python 3.5.x sur le Raspberry et sur votre ordinateur pour développer l'application
- Java 11
- Swing
- MySQL
- KeyCloak
- Elixir
- Phoenix

# Configuration

## Raspbery

Modifiez la ligne 12 du fichier [main.py](http://main.py) avec l'URL de l'API.

```python
caller = Caller("http://localhost:4000/api/donnees")
```

## API

## Application

Modifiez la ligne 116 du fichier [DrawerMain.java](http://drawermain.java) avec l'URL de l'API. Faites attention, il **ne** **faut pas changer le protocole :**

```python
Uri.Builder url = Uri.parse("ws://192.168.1.70:4000/socket/websocket").buildUpon();
```

# Lancement

## Raspberry

Lancez le code en tapant dans le dossier ''plantboard"

```bash
python ./main.py
```

## API

### Notifyer

Pour lancez l'API de notification, allez dans le dossier "notifyer" et tapez :

```bash
mix phx.server
```

### Application

Lancez l'application via Android Studio.
